# Ansible role to manage Passenger installation for Apache Httpd

## Introduction

[Phusion Passenger](https://www.phusionpassenger.com/) is a web application server.

This role installs the Apache Httpd module and configure your vhost to use Passenger.

This role also expects to work hand-in hand with the OSAS httpd role
(ansible-role-ah-httpd). You may call this role with the `httpd` role
variable for the vhost, or even simpler call it right after you setup
the vhost.

Here is a complete example:
```
    - name: Install web vhost
      include_role:
        name: httpd
        tasks_from: vhost
    - name: Install Passenger
      include_role:
        name: httpd_passenger
      vars:
        webapp_min_workers: 10
```

## Variables

- **webapp_min_workers**: minimum number of processes involved in publishing the application (defaults to 1)
- **webapp_env**: the application environment ('production', 'testing'… defaults to 'production')
- **webapp_root**: if the document root and the webapp files are separated, you can use this parameter to give the application directory
- **webapp_user**: UNIX user the application is working as (defaults to 'www-data')
- **webapp_group**: application group used in Passenger to isolate applications (and more specifically multiple versions of the same application)

